// Создать поле для ввода цены с валидацией.
//
//     Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.


let input = document.createElement('input');
let label = document.createElement('label');
label.textContent = 'Цена, $ ';
label.setAttribute('for','input');
input.setAttribute('id','input');

let parentElement = document.querySelector('div');

parentElement.appendChild(input);
parentElement.insertBefore(label, input);

input.addEventListener ('focus',function () {
    this.style.borderColor = 'green';
    if (document.querySelector('.alert')) {
        document.querySelector('.alert').remove();
    }
});
input.addEventListener('blur',function () {
    if (parseFloat(this.value) < 0 || isNaN(parseFloat(this.value))){
        incorrectPrice(this);
    } else {
        this.style.borderColor = '';
        addPrice(this);
        input.style.color = 'green';
    }
});

function addPrice(element) {
    let span = document.createElement('p');
    span.classList.add('price-window');
    span.textContent = (`Текущая цена:  ${element.value}  `);
    let xButton = document.createElement('button');
    xButton.textContent = 'X';
    label.before(span);
    span.insertAdjacentElement("afterbegin", xButton);
    xButton.addEventListener('click', function(){
        span.remove();
        xButton.remove();
        element.value = '';
    })
}

function incorrectPrice(element) {
            element.style.borderColor = 'red';
            let alert = document.createElement('p');
            alert.classList.add('alert');
            alert.textContent = "Пожалуйста введите корректную цену!";
            alert.style.color = 'red';
            element.insertAdjacentElement('afterend',alert);
}